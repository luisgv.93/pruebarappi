//
//  peliculaAccesBD.swift
//  ExamenRappi
//
//  Created by Luis ramon Gomez vargas on 03/04/22.
//

import Foundation
import CoreData
class peliculaAccesBD{
    var context:NSManagedObjectContext?
    func initcontext(contex:NSManagedObjectContext){
        self.context = contex
    }
    func insertarPeliculas(listaPeliculas:[TMoviesDTO]){
        self.borrarPeliculasCategoria(listaPeliculas[0].categoria)
        do{
            for pelicula in listaPeliculas{
                let entity = NSEntityDescription.entity(forEntityName: "TPeliculas", in: context!)
                let PeliCD = NSManagedObject(entity: entity!, insertInto: context!)
                PeliCD.setValue(pelicula.descripcion, forKeyPath: "fc_resumen")
                PeliCD.setValue(pelicula.titulo, forKeyPath: "fc_titulo")
                PeliCD.setValue(pelicula.categoria, forKeyPath: "fc_categoria")
                PeliCD.setValue(pelicula.id, forKeyPath: "fi_id")
                PeliCD.setValue(pelicula.pathImg, forKey: "fc_rutaimagen")
                 context!.insert(PeliCD)
            }
            try context!.save()
        }catch let error as NSError{
            print("No se pudo guardar el registro. \(error)")
        }
    }
    func borrarPeliculasCategoria(_ categoria:String){
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TPeliculas")
        do{
            let movies = try context!.fetch(fetchRequest) as! [TPeliculas]
            for movie in movies{
                if movie.fc_categoria! == categoria{
                    
                    context!.delete(movie)
                }
            }
            try context!.save()
        }catch{}
    }
    func getPeliculas(_ catgoria:String)->[TMoviesDTO]{
        var listaPeliculas = [TMoviesDTO]()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "TPeliculas")
        do{
            let peliculasBase = try context!.fetch(fetchRequest) as! [TPeliculas]
            for pelicula in peliculasBase{
                let objPelicula = TMoviesDTO()
                if pelicula.fc_categoria! == catgoria{
                    objPelicula.descripcion = pelicula.fc_resumen!
                    objPelicula.titulo = pelicula.fc_titulo!
                    objPelicula.categoria = pelicula.fc_categoria!
                    objPelicula.id = Int(pelicula.fi_id)
                    objPelicula.pathImg = pelicula.fc_rutaimagen!
                    listaPeliculas.append(objPelicula)
                }
                
            }
        }catch{
            
        }
        return listaPeliculas
    }
}
