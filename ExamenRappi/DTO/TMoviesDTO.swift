//
//  TMoviesDTO.swift
//  ExamenRappi
//
//  Created by Luis ramon Gomez vargas on 30/03/22.
//

import Foundation
class TMoviesDTO {
    var id:Int=0
    var titulo:String=""
    var pathImg:String=""
    var descripcion:String=""
    var categoria:String=""
}
struct TCategoriasDTO{
    var nombre:String
    var peliculas:[TMoviesDTO]
}
