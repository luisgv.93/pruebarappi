//
//  Util.swift
//  ExamenRappi
//
//  Created by Luis ramon Gomez vargas on 03/04/22.
//

import Foundation
import UIKit
class Util{
    static let shared = Util()
    func guardarImagen(_ data :Data,_ nombre:String){
        guard let directorio = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else{
            return
        }
        let fileUrl = directorio.appendingPathComponent(nombre)
        do{
            try data.write(to: fileUrl)
        }catch{}
    }
    func recuperarImagen(_ nombre:String)->UIImage?{
        let directorio = FileManager.SearchPathDirectory.documentDirectory
        let domainMask = FileManager.SearchPathDomainMask.userDomainMask
        let rutas = NSSearchPathForDirectoriesInDomains(directorio, domainMask, true)
        if let path = rutas.first{
            let rutaImagen = URL(fileURLWithPath: path).appendingPathComponent(nombre)
            let imagen = UIImage(contentsOfFile: rutaImagen.path)
            return imagen
        }
        return nil
    }
}
