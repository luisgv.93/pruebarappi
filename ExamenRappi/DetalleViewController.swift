//
//  DetalleViewController.swift
//  ExamenRappi
//
//  Created by Luis ramon Gomez vargas on 03/04/22.
//

import UIKit
import WebKit
class DetalleViewController: UIViewController {
    @IBOutlet weak var vide:WKWebView!
    @IBOutlet weak var titulo:UILabel!
    @IBOutlet weak var resumen:UILabel!
    
    var pelicula = TMoviesDTO()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setData()
        self.getVideo()

        // Do any additional setup after loading the view.
    }
    func setData(){
        self.titulo.text = pelicula.titulo
        self.resumen.text = pelicula.descripcion
    }
    func getVideo() {
        Request.doRequest.getVideos(idPelicula: pelicula.id) { idVideo in
            if idVideo != nil{
                let url = URL(string: "https://www.youtube.com/embed/"+idVideo!)
                DispatchQueue.main.async {
                    
                    self.vide.load(URLRequest(url: url!))
                }
            }
         
        }
    }
}
