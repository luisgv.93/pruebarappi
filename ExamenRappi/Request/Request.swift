//
//  Request.swift
//  ExamenRappi
//
//  Created by Luis ramon Gomez vargas on 29/03/22.
//

import Foundation
import UIKit
class Request{
    static let  doRequest = Request()
    func getPeliculas(categoria:String,page:Int,completionHandler:@escaping(_ response:[TMoviesDTO]?)->()){
        var listaTopMovies=[TMoviesDTO]()
        let rutaCategoria = self.getString(cat: categoria)
        let urlTopMovies = URL(string: "https://api.themoviedb.org/3/movie/"+rutaCategoria+"?api_key=4e14d90310a0180d492a716d598cdacf&language=es&page="+String(describing:page))
        let request = URLSession.shared.dataTask(with: urlTopMovies!){data,response,error in
            if error == nil{
                if let responseJson = try? JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary{
                    if let listaMovies = responseJson["results"] as? NSArray {
                        for movie in listaMovies{
                            let movieJson = movie as! NSDictionary
                            print(movieJson)
                            let objMovie = TMoviesDTO()
                            objMovie.descripcion = ((movieJson["overview"] as? String) != nil) ? movieJson["overview"] as! String : ""
                            objMovie.pathImg = ((movieJson["poster_path"] as? String) != nil) ? movieJson["poster_path"] as! String : ""
                            objMovie.titulo = ((movieJson["title"] as? String) != nil) ? movieJson["title"] as! String : ""
                            objMovie.id = ((movieJson["id"] as? Int) != nil) ? movieJson["id"] as! Int : 0
                            objMovie.categoria = categoria
                            listaTopMovies.append(objMovie)
                        }
                        if page == 1{
                            self.guardarBD(lista: listaTopMovies)
                        }
                        completionHandler(listaTopMovies)
                    }else{
                        completionHandler(nil)
                    }
                }else{
                    completionHandler(nil)
                }
            }else{
                completionHandler(nil)
            }
        }
        request.resume()
    }
    func getVideos(idPelicula:Int,completionHandler:@escaping(_ idVideo:String?)->()){
        let urlTopMovies = URL(string: "https://api.themoviedb.org/3/movie/"+"\(idPelicula)"+"/videos?api_key=4e14d90310a0180d492a716d598cdacf&language=en-US")
        let request = URLSession.shared.dataTask(with: urlTopMovies!){data,response,error in
            if error == nil{
                if let responseJson = try? JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary{
                    if let listaVideos = responseJson["results"] as? NSArray {
                        for video in listaVideos{
                            let videoJson = video as! NSDictionary
                            if let idVid = videoJson["key"] as? String{
                                completionHandler(idVid)
                                break
                            }
                        }
                    }else{
                        completionHandler(nil)
                    }
                }else{
                    completionHandler(nil)
                }
            }else{
                completionHandler(nil)
            }
        }
        request.resume()
    }
    private func guardarBD(lista:[TMoviesDTO]){
        DispatchQueue.main.async {
            let peliculaBD = peliculaAccesBD()
            let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            peliculaBD.initcontext(contex: context)
            peliculaBD.insertarPeliculas(listaPeliculas: lista)
        }
    }
    private func getString(cat:String)->String{
        switch cat{
        case "Top Rated":
            return "top_rated"
        case "Popular":
            return "popular"
        case "Upcoming":
            return "upcoming"
        default:
            return "top_rated"
        }
    }
}
