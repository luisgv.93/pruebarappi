//
//  ViewController.swift
//  ExamenRappi
//
//  Created by Luis ramon Gomez vargas on 28/03/22.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView:UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.llenarCategorias()
    }
    var categorias = [TCategoriasDTO]()
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categorias.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoria", for: indexPath) as! PeliculasTableViewCell
        cell.categoria = self.categorias[indexPath.row]
        cell.vcParent = self
        return cell
    }
    func llenarCategorias(){
        self.categorias.append(TCategoriasDTO(nombre: "Top Rated", peliculas: [TMoviesDTO]()))
        self.categorias.append(TCategoriasDTO(nombre: "Popular", peliculas: [TMoviesDTO]()))
        self.categorias.append(TCategoriasDTO(nombre: "Upcoming", peliculas: [TMoviesDTO]()))
    }
    
   
}



