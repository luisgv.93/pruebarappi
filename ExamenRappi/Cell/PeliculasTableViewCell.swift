//
//  PeliculasTableViewCell.swift
//  ExamenRappi
//
//  Created by Luis ramon Gomez vargas on 02/04/22.
//

import UIKit

class PeliculasTableViewCell: UITableViewCell {
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titulo:UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }
    var categoria:TCategoriasDTO?
    var vcParent:ViewController?
    var pag=1
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        collectionView.delegate = self
        collectionView.dataSource = self
        setData()
        // Configure the view for the selected state
    }
    func setData(){
        if self.categoria?.peliculas.count == 0{
            self.titulo.text = self.categoria!.nombre
            Request.doRequest.getPeliculas(categoria: self.categoria!.nombre, page: self.pag) { response in
                if response != nil{
                    self.categoria!.peliculas = response!
                    self.pag += 1
                    DispatchQueue.main.async {
                        self.collectionView.reloadData()
                    }
                }else{
                    DispatchQueue.main.async {
                        let peliculaBD = peliculaAccesBD()
                        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
                        peliculaBD.initcontext(contex: context)
                        let listaPeliculasBD = peliculaBD.getPeliculas(self.categoria!.nombre)
                        self.categoria!.peliculas.append(contentsOf: listaPeliculasBD)
                        self.pag += 1
                        self.collectionView.reloadData()
                    }
                    
                }
            }
        }
    }
    
}
extension PeliculasTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categoria!.peliculas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "pelicula2", for: indexPath) as! PeliculaCell
        let url = URL(string: "https://image.tmdb.org/t/p/w500"+categoria!.peliculas[indexPath.row].pathImg)
        if let data = try? Data(contentsOf: url!) {
            cell.itemImage.image = UIImage(data: data)
            if indexPath.row < 20{
                Util.shared.guardarImagen(data, categoria!.peliculas[indexPath.row].pathImg+".png")
            }
        }else{
            let imagen = Util.shared.recuperarImagen(categoria!.peliculas[indexPath.row].pathImg+".png")
            cell.itemImage.image = imagen != nil ? imagen! : UIImage(named: "not_found")
        }
        if indexPath.row == self.categoria!.peliculas.count - 1{
            Request.doRequest.getPeliculas(categoria: self.categoria!.nombre, page: self.pag) { response in
                if response != nil{
                self.categoria!.peliculas.append(contentsOf: response!)
                self.pag += 1
                DispatchQueue.main.async(execute: self.collectionView.reloadData)
                }
                
            }
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: nil)
        let vcDetalle : DetalleViewController = storyBoard.instantiateViewController(withIdentifier: "DetalleViewController") as! DetalleViewController
        vcDetalle.pelicula = categoria!.peliculas[indexPath.row]
        self.vcParent?.navigationController?.present(vcDetalle, animated: true)
    }
    
    
}
class PeliculaCell: UICollectionViewCell {
 
   @IBOutlet weak var itemImage: UIImageView!
    
 
}
